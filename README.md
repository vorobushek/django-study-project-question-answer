# Django Study Project - Question-Answer

The project implements the service "Question-Answer", [Stack Overflow](https://stackoverflow.com) was taken as an example of implementation :)

## Technologies used
- Application code is written in ** Python + Django **.
- The application runs under the control of the ** Gunicorn ** server.
- Database - ** MySQL **.
- ** nginx ** is used for serving statics.
- To deliver real-time messages ** nginx + mod_push **.
- For data caching - ** memcached **.
- Layout is done using ** Twitter Bootstrap **.
- User interface interaction is provided by ** JavaScript / jQuery **.
- ** django.contrib.auth ** application can be used to authorize and store users.
